import request
import telebot
import secret_data

bot = telebot.TeleBot(secret_data.api)


@bot.message_handler(commands=['start'])
def start_command(message):
    bot.send_message(
        message.from_user.id,
        'Привет! Я - Train Timetable Bot, и с моей помощью Вы можете посмотреть расписание электричек'
    )


@bot.message_handler(commands=['help'])
def help_command(message):
    bot.send_message(
        message.from_user.id,
        'Для того чтобы узнать расписание электричек на ближайшее время, введите:\n' +
        '● пункт отправления\n' + '● пункт прибытия\n' + '(каждое с новой строки)'
    )


@bot.message_handler(commands=['creator'])
def creator_command(message):
    keyboard = telebot.types.InlineKeyboardMarkup()
    keyboard.add(telebot.types.InlineKeyboardButton('Message the developer', url='telegram.me/lwtztea'))
    bot.send_message(
        message.from_user.id,
        'Для связи с разработчиком',
        reply_markup=keyboard
    )


@bot.message_handler(content_types=['text'])
def get_text_message(message):

    lines = message.text.split('\n')
    if lines[0] == "Привет":
        bot.send_message(
            message.from_user.id, 'Привет :)')
    else:
        lines.append('')
        res = request.get_request("'" + lines[0] + "'", "'" + lines[1] + "'")
        if res is None:
            text = 'По вашему запросу ничего не нашлось :(\n' + \
                'Возможен неправильный формат ввода данных\n' + \
                '(для подробностей нажмите /help)'
        else:
            text = 'Отправление {}\n'.format(res[0]) + \
                'Прибытие {}\n'.format(res[1]) + \
                'Остановки {}\n'.format(res[2]) + \
                'Стоимость {} руб.\n'.format(res[3])
        bot.send_message(message.from_user.id, text)


bot.polling(none_stop=True)
